# Persistent Memory Interface

The Persistent Memory Interface is a tool for uploading and downloading persistent configurations
for Kiteswarms firmware.

## Documentation

Documentation is available at https://kiteswarms.gitlab-pages.kiteswarms.com/persistent-memory-interface/

## Installation
The Persistent Memory Interface tool is contained in a pip package, which can be installed by
```
git clone git@gitlab.com:kiteswarms/persistent-memory-interface.git
cd persistent-memory-interface
pip install .
```

## Usage
Persistent Memory Interface comes equipped with two executables:
 `memory-uploader` and `memory-downloader`.

### `memory-uploader`
```
usage: memory_uploader.py [-h] [--default-config DEFAULT_CONFIG] [--port PORT] [--elevated ELEVATED] [--list-default-configs]
                          [board] [file]

positional arguments:
  board                 Target board name
  file                  Path to the memory file

optional arguments:
  -h, --help            show this help message and exit
  --default-config DEFAULT_CONFIG, -c DEFAULT_CONFIG
                        Use a default config.
  --port PORT, -p PORT  Pulicast port
  --elevated ELEVATED, -e ELEVATED
                        Allows to overwrite read-only values
  --list-default-configs
                        Lists all available default configs.
```
The `board` option is required and defines the target board which should be configured.
Either the `file` or `--default-config` option are required which define the configuration that is uploaded to `board`.

### `memory-downloader`
```
usage: memory-downloader [-h] [--file FILE] [--port PORT] board

positional arguments:
  board                 Target board name

optional arguments:
  -h, --help            show this help message and exit
  --file FILE, -f FILE  Path to the memory file
  --port PORT, -p PORT  Pulicast port
```

The `board` option is required and defines the target board whose configuration should be downloaded.
