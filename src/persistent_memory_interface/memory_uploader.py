#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of persistent-memory-interface.
#
#     persistent-memory-interface is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     persistent-memory-interface is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with persistent-memory-interface.  If not, see <https://www.gnu.org/licenses/>.

import os
import argparse
from .utils import memory_converter as converter
from .utils import persistent_memory_interface as iface


def main():
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument('board', type=str, nargs="?",
                        help='Target board unique hardware identifier')
    parser.add_argument('file', type=str, nargs="?", help='Path to the memory file')
    parser.add_argument('--default-config', '-c', type=str, help='Use a default config.')
    parser.add_argument('--port', '-p', type=int, default=8765, help='Pulicast port')
    parser.add_argument('--elevated', '-e', action='store_true',
                        help='Allows to overwrite read-only values')
    parser.add_argument('--list-default-configs', action='store_true',
                        help='Lists all available default configs.')
    parser.add_argument("-i", "--interface-address", default="0.0.0.0",
                        help="Ip address of the interface pulicast is suppose to send.")
    args = parser.parse_args()

    this_dir, this_filename = os.path.split(__file__)
    if args.list_default_configs:
        print("List of available default configs:")
        for config in os.listdir(os.path.join(this_dir, "default_configs")):
            print("  " + os.path.splitext(config)[0])
        exit(0)

    if not args.board:
        print("No target board specified!")
        exit(0)

    if not args.file and not args.default_config:
        print("No custom config or default config specified!")
        exit(0)

    if args.file and args.default_config:
        print("Custom config and default config specified! Choose one!")
        exit(0)

    if args.file:
        # Read human readable memory json File
        memory_human = converter.import_memory_file(args.file)
    else:
        # Read human readable memory json File
        memory_human = converter.import_memory_file(
            os.path.join(this_dir, "default_configs", args.default_config + ".json"))

    if (not memory_human):
        exit(1)

    print(memory_human)

    # Convert to machine readable
    def cb_error(msg, item):
        print("> " + msg + ": " + str(item))
    to_machine = converter.HumanToMachine(cb_error)
    memory_machine = to_machine.convert_to_machine_readable(memory_human)
    print(memory_machine)

    # Exit on conversion error
    if not memory_machine:
        print("> Conversion to machine readable format failed")
        exit(1)

    # Upload memory
    interface = iface.PersistentMemoryInterface(args.board, pulicast_port=args.port, pulicast_ttl=1, pulicast_interface=args.interface_address)
    converter.delete_flash(interface, args.elevated)
    converter.upload_dict(interface, memory_machine, to_machine.definitions, args.elevated)


if __name__ == "__main__":
    main()
