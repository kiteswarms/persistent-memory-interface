#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of persistent-memory-interface.
#
#     persistent-memory-interface is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     persistent-memory-interface is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with persistent-memory-interface.  If not, see <https://www.gnu.org/licenses/>.

import argparse
from .utils import memory_converter as converter
from .utils import persistent_memory_interface as interface


def main():
    # Parse args
    parser = argparse.ArgumentParser()
    parser.add_argument('board', type=str, help='Target board unique hardware identifier')
    parser.add_argument('--file', '-f', type=str, default="memory_export.json",
                        help='Path to the memory file')
    parser.add_argument('--port', '-p', type=int, default=8765, help='Pulicast port')
    parser.add_argument('--internal-keys', '-k', action='store_true',
                        help='Also download internal keys.')
    parser.add_argument("-i", "--interface-address", default="0.0.0.0",
                        help="Ip address of the interface pulicast is suppose to send.")
    args = parser.parse_args()

    # Download memory
    iface = interface.PersistentMemoryInterface(args.board, pulicast_port=args.port, pulicast_ttl=1, pulicast_interface=args.interface_address)
    memory_machine = iface.get_all_key_value_pairs(args.internal_keys)

    # Exit on conversion error
    if len(memory_machine) == 0:
        print("> Persistent memory is empty")
        exit(0)

    # Convert list of key, value pairs to a dictionary
    dict_machine = {}
    for key, value in memory_machine:
        dict_machine[key] = value

    # Convert to human readable
    def cb_error(msg, item):
        print("> " + msg + ": " + str(item))
    to_human = converter.MachineToHuman(cb_error)
    memory_human = to_human.convert_to_human_readable(dict_machine)
    print(memory_human)

    # Exit on conversion error
    if not memory_human:
        print("> Conversion to human readable format failed")
        exit(1)

    # store to human readable memory json file
    converter.export_memory_file(memory_human, args.file)


if __name__ == "__main__":
    main()
