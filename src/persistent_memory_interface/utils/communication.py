# !/bin/python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of persistent-memory-interface.
#
#     persistent-memory-interface is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     persistent-memory-interface is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with persistent-memory-interface.  If not, see <https://www.gnu.org/licenses/>.

import ctypes
import zcom
import pulicast


class Communication:
    """The Communication class wraps the pulicast interfaces to the persistent memory of
    mfw, mfwRpc and KiteOS and provides a unified interface to access the boards persistent memory.
    """
    # Label definitions
    _PERSISTENT_MEMORY_MANAGER_LABEL = "MEM"  # Plugin string of the persistent memory manager
    _COMMAND_LABEL = "CMD"  # Channel string of the command channel
    _RESPONSE_LABEL = "RE"  # Channel string of the response channel

    def __init__(self, response_handler, pulicast_port, pulicast_ttl,
                 pulicast_interface):
        """Constructor of the Communication class.

        :param response_handler:   Handler which called when a response was received.
        :param pulicast_port:      Pulicast port to use for communication when pulicast is used.
        :param pulicast_ttl:       TTL to use for communication when pulicast is used.
        :param pulicast_interface: Network interface for pulicast to work on.
        """
        # Store channel names in member variables
        self._communication_channel = "mem"

        # Store function parameters in member variables
        self._response_handler = response_handler
        self._pulicast_port = pulicast_port
        self._pulicast_ttl = pulicast_ttl
        self._pulicast_interface = pulicast_interface

        # Initialize pulicast
        self._pulicast_handle = pulicast.ThreadedNode("persistent_memory_editor",
                                                      self._pulicast_port, self._pulicast_ttl,
                                                      interface_address=pulicast_interface)

        # Start the background thread for package processing
        self._pulicast_handle.start()

        # Subscribe to pulicast response channel
        self._pulicast_handle[self._communication_channel].subscribe(
            self._pulicast_response_handler)

    def destroy(self):
        self._pulicast_handle.stop()

    def get_pulicast_session_id(self):
        return self._pulicast_handle.session_id

    def send(self, command):
        """Sends a command to the hardware containing the persistent memory.

        :param command: Command to send.
        """
        # Convert command to uint8 C type
        command_uint8 = []
        for element in command:
            command_uint8.append(ctypes.c_uint8(element).value)

        # Assemble message
        message = zcom.timestamped_vector_byte()
        message.values = command_uint8
        message.len = len(command)

        # Send command
        self._pulicast_handle[self._communication_channel] << message

    def _pulicast_response_handler(self, message: zcom.timestamped_vector_byte):
        """Pulicast message handler which is called when a message is received on the response
        channel.

        :param message: Received message.
        """
        self._response_handler(message.values)
