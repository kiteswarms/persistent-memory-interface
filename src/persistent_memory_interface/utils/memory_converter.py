#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of persistent-memory-interface.
#
#     persistent-memory-interface is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     persistent-memory-interface is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with persistent-memory-interface.  If not, see <https://www.gnu.org/licenses/>.
import os
import math
import json
import struct


class MachineToHuman:
    def __init__(self, cb_error):
        self.valueTypes = {
            'uint8': self.parse_uint8,
            'uint32': self.parse_uint32,
            'string': self.parse_string,
            'bool': self.parse_bool,
            'enum': self.parse_enum}
        self.definitions = MemoryDefinitions()
        self.cb_error = cb_error

    def get_value(self, definition, raw_val):
        return self.valueTypes[definition['type']](definition, raw_val)

    def get_string_description(self, value):
        chars = [*struct.unpack("4B", struct.pack(">I", value))]
        ret = ""
        for i in chars:
            if i != 0:
                ret += chr(i)
            else:
                break
        if ret == "":
            ret = "Not set"
        return ret

    def parse_uint32(self, definition, raw_val):
        return raw_val

    def parse_uint8(self, definition, raw_val):
        len_input = len(raw_val)
        len_output = len_input * 4

        struct_item = struct.pack('>' + str(len_input) + 'I', *raw_val)
        list_out = struct.unpack('>' + str(len_output) + 'B', struct_item)
        list_out = list(list_out[0:definition['len']])
        return list_out

    def parse_string(self, definition, raw_val):
        val = ""
        for byte4 in raw_val:
            val += self.get_string_description(byte4)
        return val

    def parse_bool(self, definition, raw_val):
        list_out = []
        for integer_item in raw_val:
            if type(integer_item) != int:
                self.cb_error("can not parse bool", integer_item)
                return None

            # Convert, pad and reverse
            list_bools = [b == '1' for b in bin(integer_item)[2:].rjust(4)[::1]]
            list_out += list_bools
        list_out = list(list_out[0:definition['len']])
        return list_out

    def parse_enum(self, definition, raw_val):
        dict_enums = definition['values']
        key_enum = str(raw_val[0])
        if key_enum not in dict_enums:
            self.cb_error("enum not found", raw_val)
            return None
        return dict_enums[key_enum]

    def convert_to_human_readable(self, dict_machine):
        dict_memory = {}
        for key in sorted(dict_machine.keys()):
            val = dict_machine[key]
            name, defn = self.definitions.get_definition_from_key(key)

            if name not in dict_memory:
                dict_memory[name] = []
            dict_memory[name].append(val)

        dict_human = {}
        for name, rawVal in dict_memory.items():
            if name:
                parsed_value = self.get_value(self.definitions.dict_definitions[name], rawVal)
                dict_human[name] = parsed_value
        return dict_human


class HumanToMachine:
    def __init__(self, cb_error):
        self.valueTypes = {
            'uint8': self.convert_uint8,
            'uint32': self.convert_uint32,
            'string': self.convert_string,
            'bool': self.convert_bool,
            'enum': self.convert_enum}
        self.definitions = MemoryDefinitions()
        self.cb_error = cb_error

    def get_machine_value(self, definition, val):
        return self.valueTypes[definition['type']](definition, val)

    def convert_uint8(self, definition, list_vals):
        if len(list_vals) > definition['len']:
            self.cb_error('list too large', list_vals)
            return None

        len_output = math.ceil(len(list_vals)/4)
        len_input = len_output * 4
        list_extended_vals = [0] * len_output * 4
        for i in range(len(list_vals)):
            val = list_vals[i]
            if type(val) is not int:
                self.cb_error(f'value "{str(val)}" is not a byte', list_vals)
                return None
            if list_vals[i] > 255:
                self.cb_error(f'value "{str(val)}" too large for byte', list_vals)
                return None
            list_extended_vals[i] = val

        struct_item = struct.pack('>' + str(len_input) + 'B', *list_extended_vals)
        return struct.unpack('>' + str(len_output) + 'I', struct_item)

    def convert_uint32(self, definition, val):
        if len(val) > definition['len']:
            self.cb_error("list too large", val)
            return None

        for i in range(len(val)):
            if type(val[i]) is not int:
                self.cb_error(f'value "{str(val[i])}" is not an integer', val)
                return None
            if val[i] > 4294967295:
                self.cb_error(f'value "{str(val[i])}" is too large for uint32', val)
                return None
        return val

    def convert_string(self, definition, val):
        if len(val) > definition['len']:
            self.cb_error("too many chars", val)
            return None

        if type(val) is not str:
            self.cb_error('value is not a string', val)
            return None

        len_output = math.ceil(len(val)/4)
        len_input = len_output * 4
        list_input = [0] * len_input
        for i in range(len(val)):
            list_input[i] = ord(val[i])

        struct_item = struct.pack(str(len_input) + "B", *list_input)
        return struct.unpack(">" + str(len_output) + "I", struct_item)

    def convert_bool(self, definition, list_vals):
        if len(list_vals) > definition['len']:
            self.cb_error('list too large', list_vals)
            return None

        list_chunks = list(get_chunks(list_vals, 32))  # split into chunks of 32
        list_output = []

        for list_cunk in list_chunks:
            for i in range(len(list_cunk)):
                val = list_cunk[i]
                if type(val) is not bool:
                    self.cb_error(f'value "{str(val)}" is not a bool', val)
                    return None

            int_chunk = sum([b << (31 - i) for i, b in enumerate(list_cunk)])
            list_output.append(int_chunk)

        return list_output

    def convert_enum(self, definition, enum_str):
        dict_enums = definition['values']
        enum_int = list(int(k) for k, v in dict_enums.items() if v == enum_str)
        if len(enum_int) == 0:
            self.cb_error("enum not found", enum_str)
            return None
        return enum_int

    # Values in this dict need to be lists. see import_memory_file() method
    def convert_to_machine_readable(self, dict_human):
        dict_machine = {}

        for key_human, value_human in dict_human.items():
            if key_human not in self.definitions.dict_definitions:
                self.cb_error("Unknown key", key_human)
                return None

            definition = self.definitions.dict_definitions[key_human]
            value_machine = self.get_machine_value(definition, value_human)

            if not value_machine:
                return None

            i = 0
            for val in value_machine:
                key = definition["keys"][i]
                dict_machine[key] = value_machine[i]
                i += 1
        return dict_machine


class MemoryDefinitions:
    def __init__(self):
        this_dir, this_filename = os.path.split(__file__)
        definition_json_file = os.path.join(this_dir, "../memory-definition.json")
        f = open(definition_json_file, "r")
        self.dict_definitions = json.loads(f.read())

    def get_definition_from_key(self, key_int):
        if type(key_int) is str:
            key_int = int(key_int, 0)
        for name, definition in self.dict_definitions.items():
            keys_int = [int(i, 0) for i in definition["keys"]]
            if key_int in keys_int:
                return name, definition
        return None, None


def get_chunks(list_item, max_len):  # chops a list into pieces with length of max_len
    for i in range(0, len(list_item), max_len): # For item i in a range that is a length of l,
        yield list_item[i:i+max_len] # Create an index range for l of n items:


def import_memory_file(file):
    f = open(file, "r")
    try:
        memory_human = json.loads(f.read())
        memory_human = inflate_list_values(memory_human)
    except json.JSONDecodeError:
        print("The file is not JSON convertible")
        memory_human = None

    f.close()
    return memory_human


def export_memory_file(dict_memory, file):
    dict_memory = reduce_list_values(dict_memory)

    with open(file, 'w') as fp:
        json.dump(dict_memory, fp, indent=4)


def upload_dict(iface, memory_machine, definitions, elevated):
    for key_string, value in memory_machine.items():
        iface.set_value(int(key_string, 0), value, elevated)


def upload_delete_definitions(iface, memory_human, definitions, elevated):
    # this deletes the given keys so there are no old values
    # in array if they are smaller than the previous ones
    for name, value in memory_human.items():
        definition = definitions.dict_definitions[name]
        for key in definition['keys']:
            iface.erase_key_value_pair(int(key, 0), elevated)


def delete_flash(iface, elevated):
    pairs = iface.get_all_key_value_pairs()
    for key, value in pairs:
        iface.erase_key_value_pair(key, elevated)


def reduce_list_values(dict_memory):
    for key, val in dict_memory.items():
        if type(val) is list and len(val) == 1:
            dict_memory[key] = val[0]
    return dict_memory


def inflate_list_values(dict_memory):
    for key, value in dict_memory.items():
        if type(value) is not str and type(value) is not tuple and type(value) is not list:
            dict_memory[key] = [value]
    return dict_memory


def get_ip_as_string(ip_as_int):
    if type(ip_as_int) is list:
        ip_as_int = ip_as_int[0]
    return ".".join([str(i) for i in [*struct.unpack("4B", struct.pack(">I", ip_as_int))]])


def get_version_as_string(version_as_int):
    # deprecated since verison ist now stored as array
    if type(version_as_int) is list:
        version_as_int = version_as_int[0]
    architectural_revision = (version_as_int & 0xFFE00000) >> 21
    layout_revision = (version_as_int & 0x001FFC00) >> 10
    schematic_revision = version_as_int & 0x000003FF
    return ".".join([str(architectural_revision), str(layout_revision), str(schematic_revision)])


def get_version_as_int(version_as_string):
    # deprecated since verison ist now stored as array
    tuple_version = version_as_string.split('.')
    architectural_revision = int(tuple_version[0], 0)
    layout_revision = int(tuple_version[1], 0)
    schematic_revision = int(tuple_version[2], 0)
    return (architectural_revision << 21) + (layout_revision << 10) + schematic_revision
