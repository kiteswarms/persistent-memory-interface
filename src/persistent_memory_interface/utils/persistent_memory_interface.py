# !/bin/python3
#     Copyright (C) 2021 Kiteswarms Ltd
#
#     This file is part of persistent-memory-interface.
#
#     persistent-memory-interface is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     persistent-memory-interface is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with persistent-memory-interface.  If not, see <https://www.gnu.org/licenses/>.

"""Contains the PersistentMemoryInterface class, which provides a interface to access a boards
persistent memory.
"""

import struct
import queue
import time

from . import communication


class NoPersistentMemoryConnectionError(Exception):
    """Exception raised when the persistent memory does not respond."""
    pass


class PersistentMemoryInterface:
    """The PersistentMemoryInterface class provides a interface to access a boards persistent
    memory.
    """

    # Message structure:
    #
    # Bytes:          0 - 22        23 - 45            46           47 - n
    #              ------------------------------------------------------------
    # Description: | Source ID | Destination ID | Message Type | Message Data |
    #              ------------------------------------------------------------
    #
    # Embedded hardware ID:
    #
    # Bytes:          0 - 15      16 - 18    19 - 22
    #              -----------------------------------
    # Description: | PCB Type | PCB Version | PCB ID |
    #              -----------------------------------

    # Message definitions
    # Message header denoting a get size command
    _MEM_MSG_GET_SIZE = 0
    # Message header denoting a get size response
    _MEM_MSG_GET_SIZE_RESPONSE = 1
    # Message header denoting a read command
    _MEM_MSG_READ = 2
    # Message header denoting a read response
    _MEM_MSG_READ_RESPONSE = 3
    # Message header denoting a read all command
    _MEM_MSG_READ_ALL = 4
    # Message header denoting a read all response
    _MEM_MSG_READ_ALL_RESPONSE = 5
    # Message header denoting a write command
    _MEM_MSG_WRITE = 6
    # Message header denoting a write response
    _MEM_MSG_WRITE_RESPONSE = 7
    # Message header denoting a erase command
    _MEM_MSG_ERASE = 8
    # Message header denoting a erase response
    _MEM_MSG_ERASE_RESPONSE = 9
    # Message header denoting an error message
    _MEM_MSG_ERROR = 255

    # Error types
    # Error type indicating that an unknown command code was received
    _MEM_ERROR_UNKNOWN_COMMAND = 0
    # Error type indicating that the received command has an unexpected length
    _MEM_ERROR_INVALID_COMMAND_LENGTH = 1

    def __init__(self, board_identifier, pulicast_port=None,
                 pulicast_ttl=None, pulicast_interface="0.0.0.0"):
        """Constructor of the PersistentMemoryInterface class.

        :param board_identifier:   Unique hardware identifier string describing the board.
        :param pulicast_port:      Pulicast port to use for communication when pulicast is used.
        :param pulicast_ttl:       TTL to use for communication when pulicast is used.
        :param pulicast_interface: Network interface for pulicast to work on.
        """
        # Create communication object
        self._communication = communication.Communication(self._response_handler, pulicast_port,
                                                          pulicast_ttl, pulicast_interface)

        # Initialize response queue
        self._response_queue = queue.Queue()

        # Convert unique board identifier string into binary representation
        if board_identifier == "Unknown_PCB":
            self._target_board_identifier = [0] * 23
        else:
            try:
                pcb_type = board_identifier.split("-")[0]
                if not 0 < len(pcb_type) <= 16:
                    raise Exception("Invalid PCB type")
                pcb_version = board_identifier.split("-")[1].split("#")[0]
                if not len(pcb_version.split(".")) == 3:
                    raise Exception("Invalid PCB version")
                pcb_id = int(board_identifier.split("-")[1].split("#")[1])
                if not 0 <= pcb_id <= 0xFFFFFFFF:
                    raise Exception("Invalid PCB ID")
                pcb_version_major = int(pcb_version.split(".")[0])
                pcb_version_minor = int(pcb_version.split(".")[1])
                pcb_version_schematic = int(pcb_version.split(".")[2])
                if (not 0 <= pcb_version_major <= 255) or (not 0 <= pcb_version_minor <= 255) or \
                        (not 0 <= pcb_version_schematic <= 255):
                    raise Exception("Invalid PCB version")
                self._target_board_identifier = [
                    *[ord(x) for x in pcb_type.ljust(16, '\0')],
                    pcb_version_major, pcb_version_minor, pcb_version_schematic,
                    *struct.unpack("4B", struct.pack(">I", pcb_id))]
            except:
                print("Invalid board ID")
                exit(1)

        pulicast_session_id = self._communication.get_pulicast_session_id()
        pulicast_session_id_list = struct.unpack("8B", struct.pack(">q", pulicast_session_id))
        self._session_id = [0] * 15
        self._session_id += pulicast_session_id_list

    def destroy(self):
        self._communication.destroy()

    def get_memory_size(self):
        """Gets the size and occupied space of the persistent memory.

        :return: size, occupied_space. size is the maximum number of key value pairs that can be
                 stored in the persistent memory as 32 bit unsigned integer. occupied_space is the
                 number of key value pairs stored in the persistent memory as 32 bit unsigned
                 integer.
        """
        # Send command
        command = [*self._session_id, *self._target_board_identifier, self._MEM_MSG_GET_SIZE]
        awaited_response = [*self._target_board_identifier, *self._session_id,
                            self._MEM_MSG_GET_SIZE_RESPONSE]
        response = self._send_command_await_response(command, awaited_response)

        # Parse response
        size = struct.unpack(">I", struct.pack("4B", *response[47:51]))[0]
        occupied_space = struct.unpack(">I", struct.pack("4B", *response[51:55]))[0]

        # Return parsed response data
        return size, occupied_space

    def get_value(self, key):
        """Gets the value corresponding to a key stored in the persistent memory.

        :param key: Key associated with the value to get. Must be a 32 bit unsigned integer.
        :return:    value, error. value is the value read from the persistent memory as 32 bit
                    unsigned integer. error is the error code returned from the device as 8 bit
                    unsigned integer.
        """
        # Send command
        command = [*self._session_id, *self._target_board_identifier, self._MEM_MSG_READ,
                   *struct.unpack("4B", struct.pack(">I", key))]
        awaited_response = [*self._target_board_identifier, *self._session_id,
                            self._MEM_MSG_READ_RESPONSE,
                            *struct.unpack("4B", struct.pack(">I", key))]
        response = self._send_command_await_response(command, awaited_response)

        # Parse response
        value = struct.unpack(">I", struct.pack("4B", *response[51:55]))[0]
        error = response[55]

        # Return parsed response data
        return value, error

    def get_all_key_value_pairs(self, internal_keys=False):
        """Gets all keys stored in the persistent memory.

        :param internal_keys: flag, true if also internal keys shall be returned.
        :return: List of key value pairs in the format [key, value]. Both values are 32 bit unsigned
                 integer.
        """
        # Send command
        command = [*self._session_id, *self._target_board_identifier, self._MEM_MSG_READ_ALL]
        awaited_response = [*self._target_board_identifier, *self._session_id,
                            self._MEM_MSG_READ_ALL_RESPONSE]
        response = self._send_command_await_response(command, awaited_response)

        # Parse number of key value pairs
        number_of_values = struct.unpack(">I", struct.pack("4B", *response[47:51]))[0]

        # Parse remaining message
        # Maximum number of values transmitted in a single read all response message
        values_per_message = 10
        key_value_pairs = []
        values_decoded = 0

        while True:
            # Parse key value pair start index
            start_index = struct.unpack(">I", struct.pack("4B", *response[51:55]))[0]

            # Parse key value pairs
            for i in range(0, min(number_of_values - values_decoded, values_per_message)):
                key = struct.unpack(">I", struct.pack("4B", *response[(i * 8 + 55):(i * 8 + 59)]))
                value = struct.unpack(">I", struct.pack("4B", *response[(i * 8 + 59):(i * 8 + 63)]))
                if (internal_keys or not isKthBitSet(key[0], 31)):  # check if internal bit is set
                    key_value_pairs.append([*key, *value])
                values_decoded += 1

            # Check if all key value pairs were received and return them if so
            if values_decoded >= number_of_values:
                return key_value_pairs

            # Still key value pairs to receive, await next message
            awaited_response = [
                *self._target_board_identifier, *self._session_id, self._MEM_MSG_READ_ALL_RESPONSE,
                *struct.unpack("4B", struct.pack(">I", number_of_values)),
                *struct.unpack("4B", struct.pack(">I", start_index + values_per_message))]
            response = self._await_response(awaited_response)

    def set_value(self, key, value, ignore_fuse_bit):
        """Sets the value corresponding to a key.

        :param key:             Key associated with the value to write to the persistent memory as
                                32 bit unsigned integer.
        :param value:           Value to write to the persistent memory as 32 bit unsigned integer.
        :param ignore_fuse_bit: This is a boolean. The hardware ignores the fuse bit of write
                                protected values if true.
        :return:                Error code returned from the device as 8 bit unsigned integer.
        """
        # Send command
        command = [*self._session_id, *self._target_board_identifier, self._MEM_MSG_WRITE,
                   *struct.unpack("4B", struct.pack(">I", key)),
                   *struct.unpack("4B", struct.pack(">I", value)),
                   *struct.unpack("B", struct.pack("?", ignore_fuse_bit))]
        awaited_response = [*self._target_board_identifier, *self._session_id,
                            self._MEM_MSG_WRITE_RESPONSE,
                            *struct.unpack("4B", struct.pack(">I", key)),
                            *struct.unpack("4B", struct.pack(">I", value))]
        response = self._send_command_await_response(command, awaited_response)

        # Parse response
        error = response[55]

        # Return parsed response data
        return error

    def erase_key_value_pair(self, key, ignore_fuse_bit):
        """Erases a key value pair from the persistent memory.

        :param key:             Key of the key value pair to erase.
        :param ignore_fuse_bit: This is a boolean. The hardware ignores the fuse bit of write
                                protected values if true.
        :return:                Error code returned from the device as 8 bit unsigned integer.
        """
        # Send command
        command = [*self._session_id, *self._target_board_identifier, self._MEM_MSG_ERASE,
                   *struct.unpack("4B", struct.pack(">I", key)),
                   *struct.unpack("B", struct.pack("?", ignore_fuse_bit))]
        awaited_response = [*self._target_board_identifier, *self._session_id,
                            self._MEM_MSG_ERASE_RESPONSE,
                            *struct.unpack("4B", struct.pack(">I", key))]
        response = self._send_command_await_response(command, awaited_response)

        # Parse response
        error = response[51]

        # Return parsed response data
        return error

    def _send_command_await_response(self, command, expected_response, timeout=1.0, max_resends=5):
        """Sends an in application programmer command to the given zcm channel and waits for the
        response.

        :param command:           In application programmer command to send. Must be a list of 8 bit
                                  unsigned integer values.
        :param expected_response: Partial message which must be the beginning of the response.
                                  Messages not starting with this values are rejected. Must be a
                                  list of 8 bit unsigned integer values.
        :param timeout:           Timeout in seconds.
        :param max_resends:       Maximum number of resends on timeout.
        :return:                  Received response.
        """
        # Send command
        self._send_command(command)
        response = self._await_response(expected_response, timeout)

        # Resend if necessary
        while response is None:
            if max_resends > 0:
                # Resend command
                max_resends -= 1
                self._send_command(command)
                response = self._await_response(expected_response, timeout)
            else:
                # Maximum number of resends reached, raise exception
                raise NoPersistentMemoryConnectionError("Target is not responding.")

        return response

    def _await_response(self, expected_response, timeout=1.0):
        """Waits for a response from the in application programmer.

        :param expected_response: Partial message which must be the beginning of the response.
                                  Messages not starting with this values are rejected. Must be a
                                  list of 8 bit unsigned integer values.
        :param timeout:           Maximum time to wait for a response.
        :return:                  Received response on success and "None" on timeout.
        """
        # Store start time
        start_time = time.time()

        # Await response
        while time.time() - start_time < timeout:
            if not self._response_queue.empty():  # Message received
                received_message = self._response_queue.get()
                # Check if the message is the expected response message
                if len(received_message) >= len(expected_response):
                    if received_message[:len(expected_response)] == expected_response:
                        # Return response
                        return received_message
            time.sleep(0.0001)

        # Timeout reached
        return None

    def _send_command(self, command):
        """Sends an in application programmer command to the given zcm channel.

        :param command: In application programmer command to send. Must be a list of 8 bit unsigned
                        integer values.
        """
        # Send command
        self._communication.send(command)

    def _response_handler(self, response):
        """Handler called when a response is received.

        :param response: Received response.
        """
        # Write received message to response queue
        self._response_queue.put(list(response))


def isKthBitSet(n, k):
    if n & (1 << (k - 1)):
        return True
    return False
