# Changelog

Until there is a dedicated guide how to use this log, please stick to this article: https://keepachangelog.com/de/0.3.0/
Please pick from the following sections when categorizing your entry:
`Added`, `Changed`, `Fixed`, `Removed`

Before every entry put one of these to mark the severity of the change:
`Major`, `Minor` or `Patch`

## Unreleased

## [2.8.0] - 23.07.2021
### Added
- [Minor] Added use_stepper_motor_plugin key to memory definitions.
### Changed
- [Patch] Changed pipeline for open sourcing.

## [2.7.1] - 07.06.2021

### Changed
- [Patch] Marked PTP synchronization interval key as deprecated.

### Fixed
- [Patch] Import statements in the terminal scripts fixed.

## [2.7.0] - 27.05.2021

### Added
- [Minor] Added use_hygrometer_plugin key to memory definitions.

## [2.6.0] - 26.05.2021

### Added
- [Minor] Added use_adc_plugin key to memory definitions.

## [2.5.0] - 28.04.2021

### Added
- [Minor] Added internal keys arg to memory downloader and changed stay_in_bootloader and firmware_broken keys to represent internal keys.
- [Minor] Added switch hardware key to memory definitions.
- [Minor] Added nucleus default configuration.
- [Patch] Added baro plugin to Fabo default configuration.
- [Patch] Added KSZ8563R to Axon default configuration.

### Changed
- [Patch] Board index is not deprecated anymore.


## [2.4.0] - 13.04.2021

### Changed
- [Minor] improved argparse.

### Added
- [Minor] Added system_time_mode and ptp_synchronization_interval keys to memory definitions.
- [Patch] Added merge request template.


## [2.3.0] - 22.03.2021

### Added
- [Minor] Added stay_in_bootloader and firmware_broken keys to memory definitions.


## [2.2.0] - 19.03.2021

### Added
- [Minor] Added use_baro_plugin key.


## [2.1.0] - 18.03.2021

### Added
- [Patch] Added UseUwbPlugin key.
- [Minor] Added new model parameter enum: KS0 ULW 003.


## [2.0.2] - 04.03.2021

### Fixed
- [Patch] Fixed missing sphinx package for docu.


## [2.0.1] - 04.03.2021

### Fixed
- [Patch] Fixed missing include prefix.


## [2.0.0] - 04.03.2021

### Added
- [Minor] Added optional default configs for different boards.
- [Patch] Added instruction to installation and usage to README.md.
- [Patch] Added Standards and Examples to Docu pages.

### Changed
- [Major] Persistent memory interface now identifies the target board by using the unique hardware identifier instead of the board name.
- [Patch] ip_address, gateway_address and netmask are uint8 arrays instead of uint32.

### Fixed
- [Patch] Fixed crash when trying to upload a bad json file.


## [1.5.1] - 01.03.2021

### Added
- [Patch] Added documentation page with rendered table.


## [1.5.0] - 22.02.2021

### Added
- [Minor] Added use_acl_plugin key to memory-definition.

### Changed
- [Patch] Replace GPS name with more proper GNSS name.


## [1.4.2]

### Changed
- [Patch] delete fram before uploading


## [1.4.1]

### Fixed
- [Patch] fixed wrong key name "use_power_manager_plugin" to "use_power_monitor_plugin"


## [1.4.0]

### Added
- [Minor] Added destroy method.
- [Minor] Added delete memory method.


## [1.3.0]

### Added
- [Minor] Added use_power_manager_plugin key to memory-definition.


## [1.2.2]

### Changed
- [Patch] Fixed "eusNucl" bug by sorting keys before assembling a string


## [1.2.1]

### Changed
- [Patch] Fixed the keys for pcb_version and pcb_version_old


## [1.2.0]

### Changed
- [Minor] changed to python 3.8, pulicast 1.1.0 and removed zcm


## [1.1.0] - 12.01.2021

### Added
- [Minor] Added use_ine_plugin key to memory-definition.


## [1.0.0]

### Changed
- [Major] End of beta. Can now be used for external tools.
- [Minor] Added elevated mode and moved some code into functions to be used by external tools.


## [0.2.6]

### Fixed
- [Patch] fixed memory downloader typo causing crash
- [Patch] moved memory downloader/uploader routines in main function to satisfy setup.py entry point


## [0.2.5]

### Fixed
- [Patch] fixed path of included memory-definition.json


## [0.2.4]

### Fixed
- [Patch] fixed copy of memory-definition.json


## [0.2.3]

### Fixed
- [Patch] fixed typo in entry point


## [0.2.2]

### Fixed
- [Patch] fixed typo in entry point
- [Patch] changed min python version from 3.8 to 3.6


## [0.2.1]

### Fixed
- [Patch] fixed entry points of pip package


## [0.2.0]

### Added
- [Minor] Added memory uploaded and downloader
- [Minor] Added memory converter
- [Minor] Added memory interface
- [Minor] Make pip installable


## [0.1.2](https://code.kiteswarms.com/kiteswarms/persistent-memory-definitions/compare/v0.1.0...v0.1.2) - 14.12.2020

### Fixed
- [Patch] Fixed version parsing


## [0.1.0](https://code.kiteswarms.com/kiteswarms/persistent-memory-definitions/-/tree/v0.1.0) - 14.12.2020

### Added
- [Minor] Initial Release
