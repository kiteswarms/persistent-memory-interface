from setuptools import setup, find_packages

setup(
    name="kiteswarms-persistent-memory-interface",
    setup_requires=["setuptools_scm", "setuptools"],
    use_scm_version=True,
    install_requires=[
        'kiteswarms-pulicast>=1.1.0'
    ],
    entry_points="""
        [console_scripts]
        memory-downloader=persistent_memory_interface.memory_downloader:main
        memory-uploader=persistent_memory_interface.memory_uploader:main
    """,
    python_requires=">=3.8",
    # Packages to add: find_packages provided by setuptools finds all packages located in subfolders.
    include_package_data=True,
    package_dir={"": "src"},
    # Packages to add: find_packages provided by setuptools finds all packages located in subfolders.
    packages=find_packages("src"),
    package_data={
        'persistent_memory_interface': ['*.json'],
    },
    # Some meta date for Debian compliance
    url="https://code.kiteswarms.com/kiteswarms/persistent-memory-interface",
    license="",  # NO LICENSE!!
    author="Julian Reimer",
    author_email="julian@kiteswarms.com",
    description="persistent-memory-interface",
    long_description="persistent-memory-interface",
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
)
