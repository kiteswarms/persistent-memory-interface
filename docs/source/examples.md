# Examples

This page contains examples on how config `.json` files should be written.

```json
{
    "kite_name": "L1K1",
    "board_name": "FA2",
    "use_acl_plugin": true,
    "pcb_type": "Fabo",
    "pcb_version": [1,2,0],
    "pcb_id": 1,
    "model_parameter_set": "KS0 ULW 001"
}

```

As we can see in the [key definitions table](definition-tables.html#key-definitions) the kite and board names cannot be longer than 4 characters. The nulltermination is automatically handled and must not be handled by the user.
As we can see the `model_parameter_set` entry does not take the index number but the name of the enumerator key which is defined in the corresponding [enumerator table](definition-tables.html#enumerator-definitions-for-model-parameter-set).
