***************************
Persistent Memory Standards
***************************

This document defines a structure, how data is stored in the persistent memory of the kiteswarms
microcontroller hardware. The concepts described in this document are independent of the type of
memory used as well as the underlying implementation in the firmware.

How data is stored
##################

Anything stored in the persistent memory is stored in key value pairs. Once a key is defined,
everything necessary to interpret the value has to be defined as well. From there on, the definition
of this value is fixed and must not be changed.

If a value should be interpreted in a different way, or changes the format it is stored in, a new
key is defined for this value. This ensures, that old values stored in the persistent memory are not
accidentally interpreted wrong by newer firmware versions.

The key is 32 bit long and uniquely identifies a value to be stored. Bit 31 (0x80000000) of the key
is used as a fuse bit. The values corresponding to keys with this bit set, can only be written once.
When such a value is written the first time, it becomes read only. Keys with true fuse bit are
intended to be used for data that never changes, like the serial number, board type or hardware
version.
Bit 30 (0x40000000) of the key is used to indicate internal use and should not be set by the user. 

The keys 0x00000000 and 0xFFFFFFFF are reserved. These keys may be used by the firmware to identify
an empty memory slot.

Key definitions
###############

Based on th persistent memory keys, ``memory-definition.json`` defines more abstract values, that
can be stored in the persistent memory. Every value definition requires a unique name used as key
for the dictionary containing all values. Every dictionary entry consist of an info string, a data
type, a length, a flag indicating if the value is deprecated and a list of used persistent memory
keys.

The info string, describes what this value means. If additional information is needed to define how
the value has to be interpreted, this is also explained here.

Every value except enum is an array of a primitive datatype per definition. The type defines the
datatype of the values, the length the number of individual values. Enums do not have a length
parameter. Instead, the possible values are specified in a values field which contains a dictionary.
The values supported, are listed in the table below.

+-----------+---------------------------------------------------------------+
| Data type | Description                                                   |
+===========+===============================================================+
|   uint8   | Array of 8 bit unsigned integer values                        |
+-----------+---------------------------------------------------------------+
|  uint16   | Array of 16 bit unsigned integer values                       |
+-----------+---------------------------------------------------------------+
|  uint32   | Array of 32 bit unsigned integer values                       |
+-----------+---------------------------------------------------------------+
|  uint64   | Array of 64 bit unsigned integer values                       |
+-----------+---------------------------------------------------------------+
|   int8    | Array of 8 bit signed integer values                          |
+-----------+---------------------------------------------------------------+
|   int16   | Array of 16 bit signed integer values                         |
+-----------+---------------------------------------------------------------+
|   int32   | Array of 32 bit signed integer values                         |
+-----------+---------------------------------------------------------------+
|   int64   | Array of 64 bit signed integer values                         |
+-----------+---------------------------------------------------------------+
|   bool    | Array of 1 bit binary values                                  |
+-----------+---------------------------------------------------------------+
|  string   | Array of 8 bit character values                               |
+-----------+---------------------------------------------------------------+
|   float   | Array of 32 bit signed floating point values                  |
+-----------+---------------------------------------------------------------+
|  double   | Array of 64 bit signed floating point values                  |
+-----------+---------------------------------------------------------------+
|   enum    | Single unsigned integer value where every value has a meaning |
+-----------+---------------------------------------------------------------+

If a primitive datatype should be stored, it is stored as an array of the length 0. If the array is
larger than 32 bit, multiple keys are needed. These keys are listed in the keys field. For values
needing multiple keys, these keys should be consecutive. The first element of an array is stored in
the most significant bits of a key. For an array of 2 uin8, this would mean, bit 31 - 24 are
value[0], bit 16 - 23 are value[1] and bit 0 - 15 are not used. if multiple keys are needed, the
value with the lowest index is stored in the lowest key and the value with the highest index in the
highest key. For 64 bit data types, the 32 most significant bits are stored in the key with the
lower index and the 32 least significant bits are stored in the key with the higher index.

The deprecated field, indicates that a value should not be used any more by new implementations. The
Info string may be extended by a hint which values should be used instead when a key is marked as
deprecated.

A string should be terminated with 0. If the length of the string stored in the value is a multiple
of 4, the string can also be terminated by not storing the next key in the persistent memory.
