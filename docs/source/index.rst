.. Persistent Memory Interface documentation master file, created by
   sphinx-quickstart on Tue Apr 14 17:35:58 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Persistent Memory Interface's documentation!
=========================================================

The Persistent Memory Interface is a tool for uploading and downloading persistent configurations
for Kiteswarms firmware.

The **module architecture** and all related topics are explained in the following:

.. toctree::
  :maxdepth: 2
  :caption: Contents:

  definition-tables
  persistent_memory_standards
  persistent_memory_communication_protocol
  examples

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
