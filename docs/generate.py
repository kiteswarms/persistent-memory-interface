import os
import json
import pandas as pd
from pytablewriter import MarkdownTableWriter

def main():
  this_dir, this_filename = os.path.split(__file__)
  definition_json_file = os.path.join(this_dir, "../src/persistent_memory_interface/memory-definition.json")
  f = open(definition_json_file, "r")
  # load json to dict
  definitions_dict = json.loads(f.read())
  # create dataframe from dict
  df = pd.DataFrame.from_dict(definitions_dict, orient='index')
  # replace Nan with ''
  df = df.fillna('')
  # make index column a normal column (to give it a name)
  df = df.reset_index()
  # Rename the columns
  df =df.rename( columns={'index' :'key', 'keys' : 'machine_keys', 'values': 'enumerators'})
  # define the new column order
  df = df.reindex(columns=["key", "type", "len", "deprecated", "machine_keys", "info", "enumerators"])

  writer = MarkdownTableWriter()
  # Generate table from persistent memory definitions (without enumerators)
  writer.from_dataframe(df.drop(columns=["enumerators"]))

  text_file = open(os.path.join(this_dir, "source/definition-tables.md") , "w")
  text_file.write("# Persistent Memory definitions\n\n")
  text_file.write("## Key Definitions\n\n")
  text_file.write(writer.dumps() + "\n")

  # Generate additional table for each enumerator
  for index, row in df.iterrows():
    if (row['type'] == "enum"):
      # read the value of the enumerator field
      df_enum = pd.DataFrame.from_dict(row['enumerators'], orient='index')
      # add the index
      df_enum = df_enum.reset_index()
      # rename column
      df_enum = df_enum.rename(columns={0 :'enum_key'})

      # Generate table from enum dataframe
      writer.from_dataframe(df_enum)
      text_file.write("## Enumerator definitions for " + row['key'] + "\n\n")
      text_file.write(writer.dumps() + "\n")

  text_file.close()

if __name__ == "__main__":
  main()
